import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DangerousGoodsComponent } from './dangerous-goods/dangerous-goods.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent },
  // {path: 'dangerous-goods', component: DangerousGoodsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
