import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-dangerous-goods',
  templateUrl: './dangerous-goods.component.html',
  styleUrls: ['./dangerous-goods.component.css']
})
// let changed;
// @Output() changed = new EventEmitter<boolean>();

export class DangerousGoodsComponent implements OnInit {
  // Select product type
  //               i. LIMITED QTY Yes No
  //                       1. Marine Poll Y/N
  //                       2. .. Y/N
  //                       3.  .. Y/N
  //                       4. .. Y/N
  //                       5. .. Y/N
  //                       6. Aerosol  Y/N
  //                               a. Level 1
  //                               b. Level 2
  //                               c. Level 3
  //                       7. Reportable Quantity Y/N
  //                       8. ID No. (UN ID, or NA Number field Dropdown)
  //                       9. Primary hazard Class (Number field Dropdown)
  //                       10. More than one class? Y/N
  //                               a. Secondary Hazrd (Number Field)
  //                               b. Tertiary Hazrd (Number Field)
  //                       11. Proper Shipping name (PSN) (open text)
  //                       12. Tech name (open text)
  //                       13. ERG# (optional) (open text)
  //                       14. CAS#  ERG# (optional) (open text)
  //                       15. Contains Liquid?
  //                               a. If yes, mark as liquid and select flashpoint
  //                               b. Flashpoint (Dropdown w 3 items)
//   //                               c. Size (number field)
//   ii.Fuly regulated
//   Same 15 bullet point fields as above (from  i. )
// iii. Lithium Battery?
//           1. No - move on
//           2. Yes, select battery type ( dropdown )
//                   a Cell
//                   b Battery
//                   c Button
//           3. Select Lithium Content
//                   a. Lithium Metal
//                           i. Enter content in grams ( open text )
//                           ii. Batter Config
//                                   i. Lithium Ion Stand Alone
//                                           i. Mark as P1965
//                                   ii. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   iii. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   iv. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   v. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   vi. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   vii. Lith Ion packed With:
//                                           i. Mark as P1967
//                   b. Lithium Ion
//                           i. Enter content in grams ( open text )
//                           ii. Batter Config
//                                   i. Lithium Ion Stand Alone
//                                           i. Mark as P1965
//                                   ii. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   iii. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   iv. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   v. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   vi. Lith Ion packed With:
//                                           i. Mark as P1967
//                                   vii. Lith Ion packed With:
//                                           i. Mark as P1967
//                   iv. Special Storage?
//                           1. If no, process complete
//                           2. If yes, Select one or multiple
//                                   a. Climate control (select one)
//                                           i. Cold Chain (mark as cold chain)
//                                           ii. Cool
//                                           iii. Room
//                                           iv. Controlled
//                                           v. Controlled
//                                           vi. Excessive Heat
//                                   b. Check box
//                                   c. Check box                                                     
//                                   d. Check box                                                     
//                                   e. Check box                                                     
//                                   f. Check box                                                     

  productList = ["Product 1", "Product 2", "Product 3"]; // What actually are the product types, this seems to 
  labels = ["Marine Poll?", "...?", "...?", "...?", "Aresol?", "Reportable Quanity"]
  idList = ["UN Id", "NA Number"]
  flashpoint = ["option 1", "option 2", "option 3"]
  aresolLevelList = ["Level 1", "Level 2", "Level 3"]
  primaryHazardClass = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  checked: boolean;
  selectedType: string | null;
  aresolType: string | null;
  isHazardous: boolean;
  isLimitedQuantity: boolean;
  isFullyRegulated: boolean;
  isLithiumBattery: boolean;
  isSpecialStorage: boolean;
  isLiquid: boolean;
  liquid: string;
  isMoreThan: boolean;
  isOption1: boolean;
  isOption2: boolean;
  isOption3: boolean;
  isMarinePoll: boolean;
  isAresol: boolean;
  form = new FormGroup({
    consumable: new FormControl('', Validators.required),
    item: new FormControl('', Validators.required),
  });
  get f(){
    return this.form.controls;
  }
  constructor() { }

  ngOnInit(): void {
  }

  changeIsHazardous(e) {
    console.log(e.target.value);
    this.isHazardous =  true;
    if (e.target.id === 'isHazardous') {
      this.isHazardous = true
    } else {
      this.isHazardous = false;
    }
    return this.isHazardous;
  }
  changeIsOption1(e) {
    console.log(e.target.value);

  }
  changeIsLimited(e) {
    console.log(e.target.value);
    if(e.target.id === "isLimited" ) {
      this.isLimitedQuantity = true
    } else {
      this.isLimitedQuantity = false;
    }
    console.log(this.isLimitedQuantity);
    return this.isLimitedQuantity;
  }

  changeIsFullyRegulated(e) {
    console.log(e.target.value);
    if(e.target.id === "isFullyRegulated" ) {
      this.isFullyRegulated = true
    } else {
      this.isFullyRegulated = false;
    }
    console.log(this.isFullyRegulated);
    return this.isFullyRegulated;
  }

  changeIsLithiumBattery(e) {
    console.log(e.target.value);
    if(e.target.id === "isLithiumBattery" ) {
      this.isLithiumBattery = true
    } else {
      this.isLithiumBattery = false;
    }
    console.log(this.isLithiumBattery);
    return this.isLithiumBattery;
  }

  changeIsSpecialStorage(e) {
    console.log(e.target.value);
    if(e.target.id === "isSpecialStorage" ) {
      this.isSpecialStorage = true
    } else {
      this.isSpecialStorage = false;
    }
    console.log(this.isSpecialStorage);
    return this.isSpecialStorage;
  }

  changeIsAresol(e){
    console.log(e.target.value);
    if(e.target.id === "isAresol" ) {
      this.isAresol = true
    } else {
      this.isAresol = false;
    }
    console.log(this.isAresol);
    return this.isAresol;
  }
  changeIsLiquid(e) {
    console.log(e.target.value);
    if(e.target.id === "isLiquid" ) {
      this.isLiquid = true
      this.liquid = "liquid"
    } else {
      this.isLiquid = false;
    }
    console.log(this.isLiquid);
    console.log(this.isLiquid);
    return this.isLiquid, this.liquid;
  }

  changeIsMoreThan(e) {
    if(e.target.id === "isMoreThan") {
      console.log("Is more than")
      this.isMoreThan = true
    } else {
      console.log("Is not than")
      this.isMoreThan = false
    }
    return this.isMoreThan
  }
  changeProductListType(e) {
    if(e.target.value === "Product 1") {
      this.selectedType = this.productList[0];
      console.log("selectedType:", this.selectedType);
    } else if (e.target.value === "Product 2") {
      console.log(`You have selected Consumable ${this.productList[1]}`)
      this.selectedType = this.productList[1];
      console.log("selectedType:", this.selectedType);


    } else if (e.target.value === "Product 3") {
      this.selectedType = this.productList[2];
      console.log("selectedType:", this.selectedType);


    } else {
      this.selectedType = null;
      console.log("selectedType:", this.selectedType);

    }
  }

  changeAresolLevelType(e) {
    if(e.target.value === "Level 1") {
      this.aresolType = this.aresolLevelList[0];
      console.log("selectedType:", this.aresolType);
    } else if (e.target.value === "Level 2") {
      this.aresolType = this.aresolLevelList[1];
      console.log("selectedType:", this.aresolType);
    } else if (e.target.value === "Level 3") {
      this.aresolType = this.aresolLevelList[2];
      console.log("selectedType:", this.aresolType);
    } else {
      this.aresolType = null;
      console.log("selectedType:", this.aresolType);

    }
  }
  changeIdNoList(e) {
    console.log(e.target.value);
  }
  changePrimaryHazardClass(e){
    console.log(e.target.value);
  }
}
