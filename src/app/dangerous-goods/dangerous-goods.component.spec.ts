import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DangerousGoodsComponent } from './dangerous-goods.component';

describe('DangerousGoodsComponent', () => {
  let component: DangerousGoodsComponent;
  let fixture: ComponentFixture<DangerousGoodsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DangerousGoodsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DangerousGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
